using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 25f;
    [SerializeField] private float swerveSpeed = 1f;
    private float _lastFrameFingerPositionX;
    private float _moveFactorX;
    private Rigidbody _rigidbody;
    private AudioSource _audioSource;
    private float count=0f;
    public float MoveFactorX => _moveFactorX;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveInput();
    }
    void MoveInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            count = count + 2; 
           if (!_audioSource.isPlaying)
           { 
               _audioSource.Play();   
           }
           _lastFrameFingerPositionX = Input.mousePosition.x;
        }
        else if (Input.GetMouseButton(0))
        {
            _moveFactorX = Input.mousePosition.x - _lastFrameFingerPositionX;
            _lastFrameFingerPositionX = Input.mousePosition.x;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            count = count - 2;
            if (_audioSource.isPlaying)
            {
                _audioSource.Stop();   
            }
            _moveFactorX = 0f;
        }
        
        float swerveAmount = Time.deltaTime * swerveSpeed * MoveFactorX * moveSpeed;
        //transform.Translate(swerveAmount, 0, moveSpeed);
        _rigidbody.AddRelativeForce(0,count,0,ForceMode.Impulse);
        transform.Rotate(0,0,swerveAmount);

        /* Vector3 clampedPosition = gameObject.transform.position;
         clampedPosition.x = Mathf.Clamp(clampedPosition.x, -50f, 50f);
         transform.position = clampedPosition;*/
    }
}
